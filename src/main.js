/* eslint-disable no-unused-vars */
// Import Vue
import Vue from 'vue';

// Import F7
import Framework7 from 'framework7/dist/framework7.esm.bundle.js';

// Import F7 Vue Plugin
import Framework7Vue from 'framework7-vue/dist/framework7-vue.esm.bundle.js';

// Import F7 Styles
import Framework7Styles from 'framework7/dist/css/framework7.css';

// Import Icons and App Custom Styles
import IconsStyles from './css/icons.css';
import AppStyles from './css/app.css';

// Import Routes
import Routes from './routes.js';

// Import App Component
import App from './app';

// Init F7 Vue Plugin
Vue.use(Framework7Vue, Framework7);

// Init App
const baseApp = new Vue({
  el: '#app',
  template: '<app/>',
  // Init Framework7 by passing parameters here
  framework7: {
    id: 'jp.dealer.carprice.co.app', // App bundle ID
    name: 'Dealer App', // App name
    theme: 'auto', // Automatic theme detection
    // App routes
    routes: Routes
  },
  statusbar: {
    scrollTopOnClick: true
  },
  // Register App Component
  components: {
    app: App
  },
  methods: {
    onDeviceReady: function() {
      /* global PushNotification */
      /* eslint no-undef: "error" */
      var push = PushNotification.init({
        android: {
          senderID: '505151738584'
        },
        browser: {
          pushServiceURL: 'http://push.api.phonegap.com/v1/push'
        },
        ios: {
          alert: 'true',
          badge: 'true',
          sound: 'true'
        }
      });

      push.on('registration', function(data) {
        console.dir(data);
        alert('Event = registration, registrationId=' + data.registrationId);
      });

      push.on('notification', function(data) {
        console.log(data);
        alert('Event = notification, message = ' + data.message);
      });

      push.on('error', function(err) {
        console.log(err);
        alert('Event = error, message = ' + err.message);
      });
    }
  }
});

document.addEventListener('deviceReady', baseApp.onDeviceReady);
